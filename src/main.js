var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 7
var array = createTruchetArray(arraySize, arraySize, 0)
var initSize = 0.1
var tileTypes = [
  [0, 0],
  [0, 1],
  [1, 0],
  [1, 1]
]
var walkers = []
var walkerTypes = []
var walkerSizes = []
var amountOfWalkers = 32
var walkerArray
var walkersToMove = []
var movementDirection = []
var previousWalkerPosition = []
var newWalkerPosition = []
var frame = 0
var moveState = false

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  walkerArray = create2DArray(arraySize + 1, arraySize + 1, 0, false)
  walkers = walkerArray.splice(0, amountOfWalkers)
  for (var i = 0; i < walkers.length; i++) {
    walkerSizes.push(1)
    if ((walkers[i][0] + walkers[i][1]) % 2 === 0) {
      walkerTypes.push(0)
    } else {
      walkerTypes.push(1)
    }
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < arraySize + 1; i++) {
    for (var j = 0; j < arraySize + 1; j++) {
      push()
      translate(Math.floor(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5) - 0.5) * boardSize * initSize), Math.floor(windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5) - 0.5) * boardSize * initSize))
      if ((i + j) % 2 === 0) {
        fill(255)
      } else {
        fill(0)
      }
      ellipse(0, 0, (boardSize * initSize))
      pop()
    }
  }

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      push()
      translate(Math.floor(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * boardSize * initSize), Math.floor(windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * boardSize * initSize))
      truchetTile(Math.floor(boardSize * initSize + 1), array[i][j][0], array[i][j][1])
      pop()
    }
  }

  for (var i = 0; i < walkers.length; i++) {
    if (walkerTypes[i] === 0) {
      fill(0)
    } else {
      fill(255)
    }
    noStroke()
    push()
    translate(Math.floor(windowWidth * 0.5 + (walkers[i][0] - Math.floor(arraySize * 0.5)) * boardSize * initSize) - boardSize * initSize * 0.5, Math.floor(windowHeight * 0.5 + (walkers[i][1] - Math.floor(arraySize * 0.5)) * boardSize * initSize) - boardSize * initSize * 0.5)
    ellipse(0, 0, boardSize * initSize * 0.5 * walkerSizes[i], boardSize * initSize * 0.5 * walkerSizes[i])
    pop()
  }

  frame += deltaTime * 0.00166
  if (frame > 1) {
    // frame = 0
    if (moveState === false) {
      walkersToMove = walkerDirections(walkers, array)
      for (var i = 0; i < walkersToMove.length; i++) {
        var rand = Math.floor(Math.random() * walkersToMove[i].length)
        if (walkersToMove[i][rand] === true) {
          movementDirection.push([i, rand])
        } else {
          movementDirection.push([-1])
        }
      }
      for (var i = 0; i < movementDirection.length; i++) {
        if (movementDirection[i].length === 2) {
          // move walker to bottom right neighbour cell
          if (movementDirection[i][1] === 0) {
            newWalkerPosition.push([walkers[movementDirection[i][0]][0] + 1, walkers[movementDirection[i][0]][1] + 1])
          }
          // move walker to bottom left neighbour cell
          if (movementDirection[i][1] === 1) {
            newWalkerPosition.push([walkers[movementDirection[i][0]][0] - 1, walkers[movementDirection[i][0]][1] + 1])
          }
          // move walker to upper right neighbour cell
          if (movementDirection[i][1] === 2) {
            newWalkerPosition.push([walkers[movementDirection[i][0]][0] + 1, walkers[movementDirection[i][0]][1] - 1])
          }
          // move walker to upper left neighbour cell
          if (movementDirection[i][1] === 3) {
            newWalkerPosition.push([walkers[movementDirection[i][0]][0] - 1, walkers[movementDirection[i][0]][1] - 1])
          }
        } else {
          newWalkerPosition.push([walkers[i][0], walkers[i][1]])
        }
      }
      previousWalkerPosition = walkers
      walkersToMove = []
      movementDirection = []
      moveState = true
      frame = 0
      setTimeout(function() {
        for (var i = 0; i < newWalkerPosition.length; i++) {
          walkers[i] = newWalkerPosition[i]
        }
        newWalkerPosition = []
        moveState = false
      }, 1000)
    }
  }

  if (moveState === true) {
    for (var i = 0; i < newWalkerPosition.length; i++) {
      walkers[i][0] += (newWalkerPosition[i][0] - previousWalkerPosition[i][0]) * frame
      walkers[i][1] += (newWalkerPosition[i][1] - previousWalkerPosition[i][1]) * frame
      walkerSizes[i] = sin(Math.PI * 0.0 + frame * Math.PI * 0.5)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function walkerDirections(walkers, array) {
  var neighbouringCells = []
  var neighbourArray = []
  for (var i = 0; i < walkers.length; i++) {
    // bottom right neighbour
    if (array[walkers[i][0]] !== undefined && array[walkers[i][1]] !== undefined) {
      neighbouringCells.push(array[walkers[i][0]][walkers[i][1]])
    } else {
      neighbouringCells.push(0)
    }
    // bottom left neighbour
    if (array[walkers[i][0] - 1] !== undefined && array[walkers[i][1]] !== undefined) {
      neighbouringCells.push(array[walkers[i][0] - 1][walkers[i][1]])
    } else {
      neighbouringCells.push(0)
    }
    // upper right neighbour
    if (array[walkers[i][0]] !== undefined && array[walkers[i][1] - 1] !== undefined) {
      neighbouringCells.push(array[walkers[i][0]][walkers[i][1] - 1])
    } else {
      neighbouringCells.push(0)
    }
    // upper left neighbour
    if (array[walkers[i][0] - 1] !== undefined && array[walkers[i][1] - 1] !== undefined) {
      neighbouringCells.push(array[walkers[i][0] - 1][walkers[i][1] - 1])
    } else {
      neighbouringCells.push(0)
    }
    neighbourArray.push(neighbouringCells)
    neighbouringCells = []
  }
  var cellDirections = []
  var directionArray = []
  for (var i = 0; i < neighbourArray.length; i++) {
    // black colored walkers
    if ((walkers[i][0] + walkers[i][1]) % 2 === 0) {
      // bottom right neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][0], tileTypes[1]))
      // bottom left neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][1], tileTypes[0]))
      // upper right neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][2], tileTypes[0]))
      // upper left neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][3], tileTypes[1]))
    }
    // white colored walkers
    if ((walkers[i][0] + walkers[i][1]) % 2 === 1) {
      // bottom right neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][0], tileTypes[3]))
      // bottom left neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][1], tileTypes[2]))
      // upper right neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][2], tileTypes[2]))
      // upper left neighbour
      cellDirections.push(arraysEqual(neighbourArray[i][3], tileTypes[3]))
    }
    directionArray.push(cellDirections)
    cellDirections = []
  }
  // returned directions [SE, SW, NE, NW]
  return directionArray
}

function truchetTile(size, type, rotation) {
  if (type === 0) {
    if (rotation === 0) {
      fill(255)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, size * 0.5)
      fill(0)
      arc(0, 0, size, size, -Math.PI, -Math.PI * 0.5)
      pop()
      push()
      translate(-size * 0.5, -size * 0.5)
      fill(0)
      arc(0, 0, size, size, 0, Math.PI * 0.5)
      pop()
    } else {
      fill(255)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, -size * 0.5)
      fill(0)
      arc(0, 0, size, size, Math.PI * 0.5, Math.PI)
      pop()
      push()
      translate(-size * 0.5, size * 0.5)
      fill(0)
      arc(0, 0, size, size, -Math.PI * 0.5, 0)
      pop()
    }
  } else {
    if (rotation === 0) {
      fill(0)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, size * 0.5)
      fill(255)
      arc(0, 0, size, size, -Math.PI, -Math.PI * 0.5)
      pop()
      push()
      translate(-size * 0.5, -size * 0.5)
      fill(255)
      arc(0, 0, size, size, 0, Math.PI * 0.5)
      pop()
    } else {
      fill(0)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, -size * 0.5)
      fill(255)
      arc(0, 0, size, size, Math.PI * 0.5, Math.PI)
      pop()
      push()
      translate(-size * 0.5, size * 0.5)
      fill(255)
      arc(0, 0, size, size, -Math.PI * 0.5, 0)
      pop()
    }
  }
}

function createTruchetArray(numRows, numCols, init) {
  var array = []
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      columns[j] = init
    }
    array[i] = columns
  }
  array[0][0] = [
    [0, 1],
    [1, 0]
  ][Math.floor(Math.random() * 2)]
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      var rand = Math.floor(Math.random() * 2)
      var direction = [-1, 1][Math.floor(Math.random() * 2)]
      if (array[i][j].length !== 2) {
        if (i !== 0) {
          if (rand === 0) {
            array[i][j] = [Math.abs(array[i - 1][j][0] + direction) % 2, array[i - 1][j][1]]
          } else {
            array[i][j] = [array[i - 1][j][0], Math.abs(array[i - 1][j][1] + direction) % 2]
          }
        } else if (j !== 0) {
          if (rand === 0) {
            array[i][j] = [Math.abs(array[i][j - 1][0] + direction) % 2, array[i][j - 1][1]]
          } else {
            array[i][j] = [array[i][j - 1][0], Math.abs(array[i][j - 1][1] + direction) % 2]
          }
        }
      }
    }
  }
  return array
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = [i, j]
      }
    }
    array[i] = columns
  }
  if (bool === false) {
    array = shuffle(array.flat(), true)
  }
  return array
}

function arraysEqual(a, b) {
  if (a === b) return true
  if (a == null || b == null) return false
  if (a.length !== b.length) return false
  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false
  }
  return true
}
